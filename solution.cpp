#include <cstdio>
#include <algorithm>
#include <vector>
#define MATRIX_SIZE 2
#define MOD 1000000007

using namespace std;

typedef long long ll;
typedef pair<ll, ll> pll;

struct Matrix {
    ll mat[MATRIX_SIZE][MATRIX_SIZE];
};

Matrix multiply(Matrix a, Matrix b) {
    Matrix ans;
    ans.mat[0][0] = (a.mat[0][0]*b.mat[0][0] + a.mat[0][1]*b.mat[1][0]) % MOD;
    ans.mat[0][1] = (a.mat[0][0]*b.mat[0][1] + a.mat[0][1]*b.mat[1][1]) % MOD;
    ans.mat[1][0] = (a.mat[1][0]*b.mat[0][0] + a.mat[1][1]*b.mat[1][0]) % MOD;
    ans.mat[1][1] = (a.mat[1][0]*b.mat[0][1] + a.mat[1][1]*b.mat[1][1]) % MOD;

    return ans;
}

pll fast_fibo(pll num, ll increment) {
    Matrix base = {{
        {1, 1},
        {1, 0}
    }};

    Matrix ans = {{
        {1, 0},
        {0, 1}
    }};


    while (increment > 0) {
        if (increment % 2 == 1) {
            ans = multiply(ans, base);
        }

        base = multiply(base, base);
        increment /= 2;
    }

    return pll((ans.mat[0][0] * num.first + ans.mat[0][1] * num.second) % MOD,
               (ans.mat[1][0] * num.first + ans.mat[1][1] * num.second) % MOD);
}

struct SegmentTree {
    vector<pll> stree;
    vector<ll> lazy;
    int size;

    SegmentTree() {
    }

    void build(pll* arr, int idx, int l, int r) {
        if (l < r) {
            int mid = (l + r) / 2;

            build(arr, 2*idx + 1, l, mid);
            build(arr, 2*idx + 2, mid + 1, r);
            stree[idx] = pll((stree[2*idx + 1].first + stree[2*idx + 2].first) % MOD,
                             (stree[2*idx + 1].second + stree[2*idx + 2].second) % MOD);
        } else {
            stree[idx] = *(arr + l);
        }
    }

    void build(pll* arr, int N) {
        size = N;
        for (int i = 0; i < 4*N; i++) {
            stree.push_back(pll(0, 0));
            lazy.push_back(0);
        }

        build(arr, 0, 0, size - 1);
    }

    void propagate(int idx) {
        lazy[2*idx + 1] += lazy[idx];
        lazy[2*idx + 2] += lazy[idx];
        lazy[idx] = 0;
    }

    void update(int idx, int l, int r, int p, int q, int x) {
        if (p <= l && r <= q){
            lazy[idx] += x;
            stree[idx] = fast_fibo(stree[idx], lazy[idx]);

            if (l == r)
                lazy[idx] = 0;
            else
                propagate(idx);
        } else {
            propagate(idx);
            int mid = (l + r) / 2;

            if (p <= mid) update(2*idx + 1, l, mid, p, q, x);
            if (q > mid) update(2*idx + 2, mid + 1, r, p, q, x);

            pll leftChild = fast_fibo(stree[2*idx + 1], lazy[2*idx + 1]);
            pll rightChild = fast_fibo(stree[2*idx + 2], lazy[2*idx + 2]);

            stree[idx] = pll((leftChild.first + rightChild.first) % MOD,
                             (leftChild.second + rightChild.second) % MOD);
        }
    }

    void update(int p, int q, int x) {
        update(0, 0, size - 1, p, q, x);
    }

    pll query(int idx, int l, int r, int p, int q) {
        if (p <= l && r <= q) {
            stree[idx] = fast_fibo(stree[idx], lazy[idx]);

            if (l == r)
                lazy[idx] = 0;
            else
                propagate(idx);

            return stree[idx];
        } else {
            propagate(idx);
            int mid = (l + r) / 2;
            pll ans = pll(0, 0);

            if (p <= mid) {
                pll tmp = query(2*idx + 1, l, mid, p, q);
                ans.first = (ans.first + tmp.first) % MOD;
                ans.second = (ans.second + tmp.second) % MOD;
            }

            if (q > mid) {
                pll tmp = query(2*idx + 2, mid + 1, r, p, q);
                ans.first = (ans.first + tmp.first) % MOD;
                ans.second = (ans.second + tmp.second) % MOD;
            }

            pll leftChild = fast_fibo(stree[2*idx + 1], lazy[2*idx + 1]);
            pll rightChild = fast_fibo(stree[2*idx + 2], lazy[2*idx + 2]);

            stree[idx] = pll((leftChild.first + rightChild.first) % MOD,
                             (leftChild.second + rightChild.second) % MOD);

            return ans;
        }
    }

    pll query(int p, int q) {
        return query(0, 0, size - 1, p, q);
    }
};

int main() {
    int n, m;
    pll arr[100100];
    scanf("%d %d", &n, &m);

    for (int i = 0; i < n; i++) {
        int x;
        scanf("%d", &x);

        arr[i] = fast_fibo(make_pair(1, 0), x - 1);
    }

    SegmentTree st;
    st.build(arr, n);

    for (int i = 0; i < m; i++) {
        int cmd, l, r;
        scanf("%d %d %d", &cmd, &l, &r);
        l--; r--;

        if (cmd == 1) {
            int x;
            scanf("%d", &x);
            st.update(l, r, x);
        } else {
            printf("%d\n", (int)st.query(l, r).first);
        }

    }
}
